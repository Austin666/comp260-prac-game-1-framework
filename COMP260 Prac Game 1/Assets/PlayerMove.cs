﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float maxSpeed = 5.0f;
    public float acceleration = 1.0f; // in metres/second/second
    public float brake = 5.0f; // in metres/second/second
    public float turnSpeed = 30.0f; // in degrees/second

    private float speed = 0.0f; // in metres/second

    public string Horiz;
    public string Vert;

    public PlayerMove playerPrefab;
    private BeeSpawner beeSpawner;
    // Use this for initialization
    void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
        // instantiate a player
        PlayerMove player = Instantiate(playerPrefab);
    }
    public float destroyRadius = 1.0f;

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis(Horiz);
        direction.y = Input.GetAxis(Vert);



        // the horizontal axis controls the turn
        float turn = Input.GetAxis(Horiz);
        // turn the car
        transform.Rotate(0, 0,- turn * turnSpeed * Time.deltaTime*speed);
        // the vertical axis controls acceleration fwd/back


        float forwards = Input.GetAxis(Vert);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                if (speed < 0)
                {
                    speed = 0;
                }
            }
            else
            {
                speed = speed + brake * Time.deltaTime;
                if (speed > 0)
                {
                    speed = 0;
                }
            }
        }

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);


        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.World);
        transform.Translate(velocity * Time.deltaTime, Space.Self);


    }
}

